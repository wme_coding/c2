//
// Created by Anton on 03.04.2020.
//
#include "queue.h"
#pragma once

class Iterator{
    Queue *queue;
    int current;

public:
    Iterator(Queue &queue);

    void start();

    void next();

    bool finish();

    int getValue();
};


