#pragma once

#include <ostream>

class Queue{
    int front{}, rear{};
    int size{};
    int **data;

public:
    Queue(int size);

    ~Queue();

    void add(int elem);

    int get();

    int pick();

    int getSize();

    void clear();

    bool isEmpty();

    void displayQueue();

    friend class Iterator;
};




