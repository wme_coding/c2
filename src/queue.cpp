//
// Created by Anton on 03.04.2020.
//
#include "../include/queue.h"

Queue::Queue(int size) {
    this->front = this->rear = -1;
    this->size = size;
    this->data = new int*[size];
    for(int i = 0; i < size; i++){
        data[i] = nullptr;
    }
}

Queue::~Queue() {
    delete [] data;
}

void Queue::add(int value) {
    if ((front == 0 && rear == size-1) ||
        (rear == (front-1)%(size-1)))
    {
        throw std::logic_error("Queue is full");
    }

    else if (front == -1) /* Insert First Element */
    {
        front = rear = 0;
        data[rear] = new int(value);
    }

    else if (rear == size-1 && front != 0)
    {
        rear = 0;
        data[rear] = new int(value);
    }

    else
    {
        rear++;
        data[rear] = new int(value);
    }
}

int Queue::get() {
    if (front == -1)
    {
        throw std::logic_error("Queue is empty");
    }

    int temp = *data[front];
    data[front] = nullptr;
    if (front == rear)
    {
        front = -1;
        rear = -1;
    }
    else if (front == size-1)
        front = 0;
    else
        front++;

    return temp;
}

int Queue::pick() {
    if (front == -1)
    {
        throw std::logic_error("Queue is empty");
    }

    int temp = *data[front];
    return temp;
}

int Queue::getSize() {
    return size;
}

void Queue::clear() {
    for(int i = 0; i < size; i++){
        data[i] = nullptr;
    }
    front = -1;
    rear = -1;
}

bool Queue::isEmpty() {
    return front == -1;
}

void Queue::displayQueue()
{
    if (front == -1)
    {
        printf("\nQueue is Empty");
        return;
    }
    printf("\nElements in Circular Queue are: ");
    if (rear >= front)
    {
        for (int i = front; i <= rear; i++)
            printf("%d ", *data[i]);
    }
    else
    {
        for (int i = front; i < size; i++)
            printf("%d ", *data[i]);

        for (int i = 0; i <= rear; i++)
            printf("%d ", *data[i]);
    }
}


