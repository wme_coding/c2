#include <iostream>

#include "../include/queue.h"
#include "../include/iterator.h"


using namespace std;

int main() {
    Queue q(5);
    Iterator iterator(q);

    // Inserting elements in Circular Queue
    q.add(14);
    q.add(22);
    q.add(13);
    q.add(-6);

    // Display elements present in Circular Queue
    iterator.start();
    while (!iterator.finish()){
        cout << iterator.getValue() << " ";
        iterator.next();
    }


    // Deleting elements from Circular Queue
    printf("\nDeleted value = %d", q.get());
    printf("\nDeleted value = %d\n", q.get());
    //Switch to pick to make a try

    iterator.start();
    while (!iterator.finish()){
        cout << iterator.getValue() << " ";
        iterator.next();
    }

    return 0;
}


