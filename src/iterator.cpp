//
// Created by Anton on 03.04.2020.
//
#include "../include/iterator.h"

void Iterator::start() {
    current = queue->front;
}

bool Iterator::finish() {
    return current - 1 == queue->rear || (current == 0 && queue->rear == queue->size - 1);
}

void Iterator::next() {
    if(current != -1){
        if(current == queue->size - 1){
            current = 0;
        } else{
            current++;
        }
    }
}

int Iterator::getValue() {
    if(current != -1){
        int temp = *queue->data[current];
        return temp;
    }
    throw std::logic_error("Start first");
}

Iterator::Iterator(Queue &queue) {
    this->queue = &queue;
    this->current = -1;
}

